import React from 'react'
import { createStackNavigator } from '@react-navigation/stack'

import OrderList from './OrderList'
import DetailedOrder from './DetailedOrder'

const Stack = createStackNavigator()

function MyOrdersScreen() {
  return (
        <Stack.Navigator>
            <Stack.Screen 
                name="Orders" 
                component={OrderList} 
            />

            <Stack.Screen 
                name="Detailed Order" 
                component={DetailedOrder} 
            />

        </Stack.Navigator>
  )
}

export default MyOrdersScreen

import React from 'react'
import { NavigationContainer } from '@react-navigation/native'
import { createDrawerNavigator } from '@react-navigation/drawer'

import HomeScreen from './Home'
import MyAccountScreen from './MyAccount'
import MyOrdersScreen from './MyOrder'

const Drawer = createDrawerNavigator()

function MainHome() {
    return (
        <Drawer.Navigator initialRouteName="Home">
            <Drawer.Screen name="Home" component={HomeScreen} />
            <Drawer.Screen name="My Orders" component={MyOrdersScreen} />
            <Drawer.Screen name="My Account" component={MyAccountScreen} />
        </Drawer.Navigator>
    )
}

export default MainHome

import React from 'react'
import { View, Text, TouchableOpacity } from 'react-native'
function ProductList({navigation}) {
    return (
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
            <Text>ProductList Page</Text>
            <TouchableOpacity onPress={() => navigation.navigate('Cart')}>
                <Text>Cart Page</Text>
            </TouchableOpacity>
        </View>
    )
}

export default ProductList

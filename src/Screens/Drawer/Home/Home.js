import React from 'react'
import { View, Text, TouchableOpacity } from 'react-native'
function Home({navigation}) {
    return (
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
            <Text>Home Page</Text>
            <TouchableOpacity onPress={() => navigation.navigate('Products')}>
                <Text>Product List Page</Text>
            </TouchableOpacity>
        </View>
    )
}

export default Home

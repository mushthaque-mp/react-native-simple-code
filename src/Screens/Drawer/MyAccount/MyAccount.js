import React from 'react'
import { View, Text, TouchableOpacity } from 'react-native'
function MyAccount({navigation}) {
    return (
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
            <Text>MyAccount Page</Text>
            <TouchableOpacity onPress={() => navigation.navigate('Address')}>
                <Text>Address Page</Text>
            </TouchableOpacity>
        </View>
    )
}

export default MyAccount

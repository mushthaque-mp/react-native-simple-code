import React from 'react'
import { createStackNavigator } from '@react-navigation/stack'

import Address from './Address'
import MyAccount from './MyAccount'

const Stack = createStackNavigator()

function MyAccountScreen() {
  return (
        <Stack.Navigator>
            <Stack.Screen 
                name="My Account" 
                component={MyAccount} 
            />

            <Stack.Screen 
                name="Address" 
                component={Address} 
            />

        </Stack.Navigator>
  )
}

export default MyAccountScreen
